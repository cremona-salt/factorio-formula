# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{# {%- set sls_config_file = tplroot ~ '.config.file' %} #}
{%- from tplroot ~ "/map.jinja" import factorio with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_user_present = tplroot ~ '.user.present' %}

include:
  - {{ sls_user_present }}

{# include: #}
  {# - {{ sls_config_file }} #}

{%- set version = factorio.manager.version %}
factorio-manager-running-downloaded:
  file.managed:
    - name: {{ factorio.user.homedir }}/manager.zip
    - source: https://github.com/OpenFactorioServerManager/factorio-server-manager/releases/download/{{ version }}/factorio-server-manager-linux-{{ version }}.zip
    - source_hash: {{ factorio.manager.hash }}
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - require:
      - sls: {{ sls_user_present }}

factorio-manager-running-zip-extracted:
  archive.extracted:
    - name: {{ factorio.user.homedir }}/manager
    - source: {{ factorio.user.homedir }}/manager.zip
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - require:
      - factorio-manager-running-downloaded

factorio-manager-running-config-managed:
  file.serialize:
    - name: {{ factorio.user.homedir }}/manager/factorio-server-manager/conf.json
    - serializer: json
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - dataset:
        username: {{ factorio.manager.username }}
        password: {{ factorio.manager.password }}
        database_file: {{ factorio.manager.database_file }}
        cookie_encryption_key: {{ factorio.manager.cookie_encryption_key }}
        settings_file: {{ factorio.manager.settings_file }}
        log_file: {{ factorio.manager.log_file }}
        rcon_pass: {{ factorio.manager.rcon_pass }}
    - require:
      - factorio-manager-running-zip-extracted

factorio-manager-running-systemd-file-managed:
  file.managed:
    - name: /etc/systemd/system/factorio-manager.service
    - source: {{ files_switch(['factorio-manager.service.tmpl'],
                              lookup='factorio-manager-running-systemd-file-managed'
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        factorio: {{ factorio | json }}

factorio-manager-running-service-running:
  service.running:
    - name: factorio-manager.service
    - enable: True
    - require:
      - factorio-manager-running-systemd-file-managed
