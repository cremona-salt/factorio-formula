# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import factorio with context %}

factorio-manager-running-zip-extracted:
  file.absent:
    - name: {{ factorio.user.homedir }}/manager

factorio-manager-running-downloaded:
  file.absent:
    - name: {{ factorio.user.homedir }}/manager.zip
