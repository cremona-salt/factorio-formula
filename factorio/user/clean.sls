# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import factorio with context %}

factorio-user-exists-factorio-user-removed:
  user.absent:
    - name: {{ factorio.user.name }}
