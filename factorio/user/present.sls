# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import factorio with context %}

factorio-user-present-factorio-user-created:
  user.present:
    - name: {{ factorio.user.name }}
    - home: {{ factorio.user.homedir }}
    - usergroup: true
