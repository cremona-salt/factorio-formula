# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_manager_clean = tplroot ~ '.manager.clean' %}
{%- from tplroot ~ "/map.jinja" import factorio with context %}

include:
  - {{ sls_manager_clean }}

factorio-package-install-server-tar-absent:
  file.absent:
    - name: {{ factorio.user.homedir }}/server.tar.gz

factorio-package-install-server-tar-extracted:
  file.absent:
    - name: {{ factorio.user.homedir }}/server
