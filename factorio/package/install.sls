# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import factorio with context %}
{%- set sls_user_present = tplroot ~ '.user.present' %}

include:
  - {{ sls_user_present }}

factorio-package-install-server-tar-downloaded:
  file.managed:
    - name: {{ factorio.user.homedir }}/server.tar.gz
    - source: {{ factorio.pkg.url }}
    - source_hash: {{ factorio.pkg.hash }}
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - require:
      - sls: {{ sls_user_present }}

factorio-package-install-server-tar-extracted:
  archive.extracted:
    - name: {{ factorio.user.homedir }}/server
    - source: {{ factorio.user.homedir }}/server.tar.gz
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - require:
      - factorio-package-install-server-tar-downloaded

factorio-package-install-server-factorio-dir-managed:
  file.directory:
    - name: {{ factorio.user.homedir }}/server
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - recurse:
      - user
      - group
      - mode
    - require:
      - factorio-package-install-server-tar-extracted

factorio-package-install-server-save-dir-managed:
  file.directory:
    - name: {{ factorio.user.homedir }}/server/factorio/saves
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - dir_mode: 750
    - file_mode: 640
    - recurse:
      - user
      - group
      - mode
    - require:
      - factorio-package-install-server-tar-extracted

factorio-package-install-server-config-dir-managed:
  file.directory:
    - name: {{ factorio.user.homedir }}/server/factorio/config
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - dir_mode: 750
    - file_mode: 640
    - recurse:
      - user
      - group
      - mode
    - require:
      - factorio-package-install-server-tar-extracted

factorio-package-install-server-mods-dir-managed:
  file.directory:
    - name: {{ factorio.user.homedir }}/server/factorio/mods
    - user: {{ factorio.user.name }}
    - group: {{ factorio.user.name }}
    - dir_mode: 750
    - file_mode: 640
    - recurse:
      - user
      - group
      - mode
    - require:
      - factorio-package-install-server-tar-extracted
